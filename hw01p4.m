clear
clf

A = [1 0 -2;0 2 0;-2 0 1];

seed = 7

rng(seed)

np = 1000;
figure(1)
hold on

for i = 1:np;
    x = randn(3,1);
    y = randn(3,1);
    z = x + 1i*y
    R = z'*A*z/(z'*z);
    
    plot(real(R),imag(R), 'k.')
end
title('Field of values')
xlabel('Re(R)')
ylabel('Im(R)')
grid on

eigenvalues = eig(A)

    
    
    
  